# codeInAction

## 介绍
代码实战集合，经验总结输出

## Spring Boot总结篇

- [2021/03/26-Spring Boot框架中使用Jackson的处理总结](https://xiaoym.gitee.io/2021/03/26/spring-boot-code-action-jackson/)
- [2021/03/03-Spring Boot框架中针对数据文件模板的下载总结](https://xiaoym.gitee.io/2021/03/03/spring-boot-common-file-download/)
- [2021/02/03-基于Servlet体系的HTTP请求代理转发Spring Boot组件](https://xiaoym.gitee.io/2021/02/03/spring-boot-servlet-gateway-compoents/)
- [2020/12/10-Spring Boot自定义starter必知必会条件](https://xiaoym.gitee.io/2020/12/10/spring-boot-self-starter/)

