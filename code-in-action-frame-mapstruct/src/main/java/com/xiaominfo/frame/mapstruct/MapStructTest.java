/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaominfo.frame.mapstruct;

import com.xiaominfo.frame.mapstruct.domain.Car;
import com.xiaominfo.frame.mapstruct.domain.CarDto;
import com.xiaominfo.frame.mapstruct.domain.CarType;
import com.xiaominfo.frame.mapstruct.mapper.CarMapper;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/05/14 11:26
 * @since:code-in-action 1.0
 */
public class MapStructTest {

    public static void main(String[] args) {
        //given
        Car car = new Car( "Morris", 5, CarType.SAMLL );
        //when
        CarDto carDto = CarMapper.INSTANCE.carToCarDto( car );
        System.out.println(carDto.toString());
    }
}
