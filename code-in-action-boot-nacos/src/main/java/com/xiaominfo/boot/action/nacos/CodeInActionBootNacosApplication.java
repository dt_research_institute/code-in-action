package com.xiaominfo.boot.action.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeInActionBootNacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeInActionBootNacosApplication.class, args);
    }

}
