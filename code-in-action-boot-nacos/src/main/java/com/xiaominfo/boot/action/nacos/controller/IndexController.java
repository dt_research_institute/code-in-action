/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaominfo.boot.action.nacos.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/04/28 15:42
 * @since:code-in-action 1.0
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @NacosValue(value = "${spring.redis.password}",autoRefreshed = true)
    private String redisPassword;

    @Value(value = "${spring.redis.password}")
    private String springPassword;



    @GetMapping("/get")
    public ResponseEntity<HashMap<String,String>> get(){
        HashMap<String,String> value=new HashMap<String, String>();
        value.put("nacosValue",redisPassword);
        value.put("springValue",springPassword);
        return ResponseEntity.ok(value);
    }
}
