/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.test;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;

import java.io.File;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/22 16:18
 * @since:code-in-action 1.0
 */
public class FileTest {

    static final long KB_SIZE=1024;

    static final long MB_SIZE=1024*KB_SIZE;


    public static void main(String[] args) {
        File file=new File("C:\\Users\\xiaoymin\\AppData\\Roaming");
        for (File childFile:file.listFiles()){
            System.out.println(childFile.getName()+":"+ FileUtil.size(childFile)/MB_SIZE);
        }
    }
}
