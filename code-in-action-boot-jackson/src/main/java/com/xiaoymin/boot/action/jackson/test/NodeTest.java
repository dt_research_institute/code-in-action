/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/04/26 17:37
 * @since:code-in-action 1.0
 */
public class NodeTest {

    public static void main(String[] args) {
        String text="{\"results\":[{\"location\":{\"id\":\"WMNT9YCY99D6\",\"name\":\"张家界\",\"country\":\"CN\",\"path\":\"张家界,张家界,湖南,中国\",\"timezone\":\"Asia/Shanghai\",\"timezone_offset\":\"+08:00\"},\"daily\":[{\"date\":\"2021-04-26\",\"text_day\":\"小雨\",\"code_day\":\"13\",\"text_night\":\"小雨\",\"code_night\":\"13\",\"high\":\"18\",\"low\":\"14\",\"rainfall\":\"0.1\",\"precip\":\"\",\"wind_direction\":\"东\",\"wind_direction_degree\":\"90\",\"wind_speed\":\"3.0\",\"wind_scale\":\"1\",\"humidity\":\"80\"},{\"date\":\"2021-04-27\",\"text_day\":\"小雨\",\"code_day\":\"13\",\"text_night\":\"多云\",\"code_night\":\"4\",\"high\":\"24\",\"low\":\"13\",\"rainfall\":\"0.6\",\"precip\":\"\",\"wind_direction\":\"东南\",\"wind_direction_degree\":\"135\",\"wind_speed\":\"3.0\",\"wind_scale\":\"1\",\"humidity\":\"77\"},{\"date\":\"2021-04-28\",\"text_day\":\"阴\",\"code_day\":\"9\",\"text_night\":\"多云\",\"code_night\":\"4\",\"high\":\"25\",\"low\":\"13\",\"rainfall\":\"0.0\",\"precip\":\"\",\"wind_direction\":\"东南\",\"wind_direction_degree\":\"135\",\"wind_speed\":\"8.4\",\"wind_scale\":\"2\",\"humidity\":\"65\"}],\"last_update\":\"2021-04-26T11:00:00+08:00\"}]}";
        ObjectMapper objectMapper=new ObjectMapper();
        try {
            JsonNode jsonNode=objectMapper.readTree(text);
            System.out.println(jsonNode.asText());
            JsonNode dailyNode=jsonNode.get("results");
            System.out.println(dailyNode.asText());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
