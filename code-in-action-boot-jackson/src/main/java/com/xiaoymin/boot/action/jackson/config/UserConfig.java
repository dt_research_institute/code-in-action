/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.config;

import com.xiaoymin.boot.action.jackson.model.UserService;
import com.xiaoymin.boot.action.jackson.model.LoginService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/20 11:31
 * @since:code-in-action-boot-jackson 1.0
 */
@Configuration(proxyBeanMethods = false)
public class UserConfig {


    @Bean
    public UserService userService(){
        return new UserService();
    }

    @Bean
    public LoginService loginService(){
        return new LoginService(userService());
    }
}
