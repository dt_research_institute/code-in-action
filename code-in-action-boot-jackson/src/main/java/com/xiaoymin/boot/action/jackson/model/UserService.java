/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.model;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/20 11:29
 * @since:code-in-action-boot-jackson 1.0
 */
public class UserService {

    public UserService(){
        System.out.println("UserService Construct Called...");
    }
    public void sayHello(){
        System.out.println("Hello");
    }
}
