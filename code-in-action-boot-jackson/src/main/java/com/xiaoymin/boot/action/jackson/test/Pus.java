/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.test;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/04/02 14:48
 * @since:code-in-action 1.0
 */
public class Pus {
    static final DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    static final DateTimeFormatter formatterDay=DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static void main(String[] args) {
        Map<Integer,String> areaMap= MapUtil.builder(1,"普陀山区域")
                .put(2,"朱家尖区域").put(3,"舟山区域").build();

        LocalDateTime firstDate=LocalDateTime.of(2021,4,1,1,0);
        LocalDateTime endDate=LocalDateTime.of(2022,4,1,1,0);

        Map<Integer,List<Integer>> records=new HashMap<>();
        records.put(0,CollectionUtil.newArrayList(44,45,47,48,49,50));
        records.put(1,CollectionUtil.newArrayList(51,52,53,54,55,57,58,59,60,61,62,63,64,66,67,68,69,70));
        records.put(2,CollectionUtil.newArrayList(71,72,74,75,76,77,78,79,80,81));
        records.put(3,CollectionUtil.newArrayList(82,84,91,92,93,94,95));
        records.put(4,CollectionUtil.newArrayList(96,97,98,99,100));
        records.put(5,CollectionUtil.newArrayList(101,102,103,104,105,106,107));
        records.put(6,CollectionUtil.newArrayList(108,109,111,112,113,114,115,116,117,118));
        boolean flag=true;
        StringBuilder sqls=new StringBuilder();
        do {
            String createTime=firstDate.format(formatter);
            System.out.println(createTime);
            String day=firstDate.format(formatterDay);
            sqls.append("-- ").append(day).append("\r\n\r\n");
            final List<Integer> indexDb=new ArrayList<>();
            for (Map.Entry<Integer,String> entry:areaMap.entrySet()){
                Integer areaId=entry.getKey();
                String areaName=entry.getValue();
                sqls.append("-- "+areaName).append("\r\n");
                List<Integer> guideUsers=random(indexDb,records);
                System.out.println("areaId:"+areaId+",areaName:"+areaName+",userId:"+CollectionUtil.join(guideUsers,","));
                AtomicInteger atomicInteger=new AtomicInteger(0);
                for (Integer userId:guideUsers){
                    sqls.append(genSql(day,userId,areaId,areaName,atomicInteger.incrementAndGet()));
                }

            }
            sqls.append("\r\n\r\n");
            firstDate=firstDate.plusDays(1L);
            if (firstDate.isAfter(endDate)){
                flag=false;
            }
        }while (flag);
        File sqlFile=new File("F:\\deploy\\data_"+RandomUtil.randomNumbers(5)+".sql");
        FileUtil.writeString(sqls.toString(),sqlFile,"UTF-8");
        System.out.println("完成");

    }

    static String genSql(String day,Integer userId,Integer areaId,String areaName,Integer sort){
        StringBuilder sql=new StringBuilder();
        String now=LocalDateTime.now().format(formatter);
        sql.append("INSERT INTO `aide_show`.`guide_scheduling`( `create_time`, `creator`, `modified_time`, `modifier`, `guide_id`, `status`, `work_date`, `sort`, `area_id`, `area_name`) VALUES (")
                .append("'"+now+"', NULL, '"+now+"', NULL, "+userId+", 'FREE', '"+day+"',"+sort+", "+areaId+", '"+areaName+"'")
                .append(");")
                .append("\r\n");

        return sql.toString();
    }


    static List<Integer> random(final List<Integer> db,Map<Integer,List<Integer>> records){
        int random=RandomUtil.randomInt(0,7);
        if (db.contains(random)){
            return random(db,records);
        }
        db.add(random);
        return records.get(random);
    }
}
