/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.api;

import com.xiaoymin.boot.action.jackson.format.Restful;
import com.xiaoymin.boot.action.jackson.model.AjaxResult;
import com.xiaoymin.boot.action.jackson.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/09 9:29
 * @since:code-in-action-boot-jackson 1.0
 */
@RestController
public class UserApplication {

    @GetMapping("/u1")
    public Restful<User> u(){
        return new Restful<>(User.buildOne(),123);
    }


    @GetMapping("/queryOne")
    public ResponseEntity<User> queryOne(){
        return ResponseEntity.ok(User.buildOne());
    }


    @GetMapping("/queryOne1")
    public ResponseEntity<User> queryOne1(){
        return ResponseEntity.ok(User.buildOne1());
    }


    @GetMapping("/element")
    public AjaxResult<List<User>> element(){
        return null;
    }
}
