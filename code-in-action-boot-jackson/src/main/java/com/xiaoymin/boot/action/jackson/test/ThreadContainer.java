/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/04/13 9:40
 * @since:code-in-action 1.0
 */
public class ThreadContainer {

    private final Map<String, ScheduledFuture<?>> scheduledFutureMap=new HashMap<>();
    public final static ThreadContainer me=new ThreadContainer();
    private ThreadContainer(){}

    public void add(String key,ScheduledFuture<?> future){
        scheduledFutureMap.put(key,future);
    }

    public ScheduledFuture<?> get(String key){
        return scheduledFutureMap.get(key);
    }


}
