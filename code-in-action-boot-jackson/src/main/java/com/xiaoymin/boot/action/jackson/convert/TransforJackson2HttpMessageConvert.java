/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.convert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xiaoymin.boot.action.jackson.format.Restful;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/26 9:45
 * @since:code-in-action 1.0
 */
public class TransforJackson2HttpMessageConvert extends MappingJackson2HttpMessageConverter {

    public TransforJackson2HttpMessageConvert() {
        super();
        ObjectMapper objectMapper=getObjectMapper().copy();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        objectMapper.setDateFormat(sdf);
        //注册一个默认类型
        registerObjectMappersForType(Restful.class,(t)->t.put(MediaType.APPLICATION_JSON,objectMapper));
        System.out.println("TransforJackson2HttpMessageConvert init...");
    }

    @Override
    protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        super.writeInternal(object, type, outputMessage);
    }
}
