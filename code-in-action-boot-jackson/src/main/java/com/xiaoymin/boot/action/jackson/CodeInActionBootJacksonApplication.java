package com.xiaoymin.boot.action.jackson;

import com.xiaoymin.boot.action.jackson.convert.TransforJackson2HttpMessageConvert;
import com.xiaoymin.boot.action.jackson.model.UserService;
import com.xiaoymin.boot.action.jackson.model.LoginService;
import com.xiaoymin.boot.action.jackson.test.TestJob;
import com.xiaoymin.boot.action.jackson.test.ThreadContainer;
import com.xiaoymin.boot.action.jackson.test.ThreadGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@EnableScheduling
@SpringBootApplication
public class CodeInActionBootJacksonApplication implements WebMvcConfigurer {

    private static Logger logger= LoggerFactory.getLogger(CodeInActionBootJacksonApplication.class);

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    }

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application=SpringApplication.run(CodeInActionBootJacksonApplication.class, args);
        Environment env = application.getEnvironment();
        String[] beans=application.getBeanNamesForType(HttpMessageConverter.class);
        ThreadPoolTaskScheduler threadPoolTaskScheduler=application.getBean(ThreadPoolTaskScheduler.class);
        Instant start=LocalDateTime.now().plusSeconds(30).atZone(ZoneId.systemDefault()).toInstant();
        final Date date=Date.from(start);
        ScheduledFuture<?> scheduledFuture= threadPoolTaskScheduler.schedule(new TestJob(), Date.from(start));
        ThreadContainer.me.add("test",scheduledFuture);
        System.out.println("success");
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //new Thread(new ThreadGet(date)).start();

  /*      MappingJackson2HttpMessageConverter jackson2HttpMessageConverter=application.getBean(MappingJackson2HttpMessageConverter.class);
        ObjectMapper objectMapper=jackson2HttpMessageConverter.getObjectMapper().copy();

        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        objectMapper.setDateFormat(sdf);
        Map<MediaType, ObjectMapper> mapperMap=new HashMap<>();
        mapperMap.put(MediaType.APPLICATION_JSON,objectMapper);

        jackson2HttpMessageConverter.registerObjectMappersForType(Restful.class, new Consumer<Map<MediaType, ObjectMapper>>() {
            @Override
            public void accept(Map<MediaType, ObjectMapper> mediaTypeObjectMapperMap) {
                mediaTypeObjectMapperMap.put(MediaType.APPLICATION_JSON,objectMapper);
            }
        });*/
        System.out.println("---------");
        System.out.println(application.getBean(UserService.class));
        System.out.println(application.getBean(LoginService.class).getUserService());
        System.out.println("---------");
        System.out.println(application.getBean(UserService.class));
        System.out.println(application.getBean(LoginService.class).getUserService());
        logger.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}\n\t" +
                        "External: \thttp://{}:{}\n\t"+
                        "Doc: \thttp://{}:{}/doc.html\n"+
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));
    }

}
