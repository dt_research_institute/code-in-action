/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/04/13 9:43
 * @since:code-in-action 1.0
 */
public class ThreadGet implements Runnable{
    final Date startDate;

    public ThreadGet(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public void run() {
        boolean flag=true;
        do {
            Date now=Date.from(Instant.now());
            System.out.println("取消任务");
            ScheduledFuture<?> scheduledFuture=ThreadContainer.me.get("test");
            if (scheduledFuture!=null){
                System.out.println("我要取消了");
                scheduledFuture.cancel(false);
            }
            if (now.compareTo(startDate)<0){
                flag=false;
            }
        }while (flag);
    }
}
