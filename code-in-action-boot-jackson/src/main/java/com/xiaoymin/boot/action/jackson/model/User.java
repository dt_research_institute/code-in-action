/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaoymin.boot.action.jackson.model;

import cn.hutool.core.util.RandomUtil;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/03/09 9:26
 * @since:code-in-action-boot-jackson 1.0
 */
public class User {

    private String name;

    private Integer age;
    private Date nowDate;

    private LocalDateTime birthday;

    private Date studyDate;

    private LocalDate workDate;

    private Calendar firstWorkDate;


    private String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public Date getStudyDate() {
        return studyDate;
    }

    public void setStudyDate(Date studyDate) {
        this.studyDate = studyDate;
    }

    public LocalDate getWorkDate() {
        return workDate;
    }

    public void setWorkDate(LocalDate workDate) {
        this.workDate = workDate;
    }

    public Calendar getFirstWorkDate() {
        return firstWorkDate;
    }

    public void setFirstWorkDate(Calendar firstWorkDate) {
        this.firstWorkDate = firstWorkDate;
    }

    public Date getNowDate() {
        return nowDate;
    }

    public void setNowDate(Date nowDate) {
        this.nowDate = nowDate;
    }

    public static User buildOne(){
        User user=new User();
        LocalDateTime now=LocalDateTime.now();
        user.setNowDate(Date.from(Instant.now()));
        user.setWorkDate(now.plusYears(25).toLocalDate());
        user.setStudyDate(Date.from(now.plusYears(5).atZone(ZoneId.systemDefault()).toInstant()));
        user.setName("姓名-"+RandomUtil.randomString(5));
        user.setAge(RandomUtil.randomInt(0,100));
        user.setBirthday(now);
        user.setFirstWorkDate(Calendar.getInstance());
        return user;
    }

    public static User buildOne1(){
        User user=new User();
        LocalDateTime now=LocalDateTime.now();
        user.setNowDate(Date.from(Instant.now()));
        user.setWorkDate(now.plusYears(25).toLocalDate());
        user.setStudyDate(Date.from(now.plusYears(5).atZone(ZoneId.systemDefault()).toInstant()));
        //user.setName("姓名-"+RandomUtil.randomString(5));
        user.setAge(RandomUtil.randomInt(0,100));
        user.setBirthday(now);
        user.setFirstWorkDate(Calendar.getInstance());
        return user;
    }
}
